import React, { Component } from 'react';
import Table from './components/Body/Table.jsx';
import Header from './components/Header/Header.jsx';
import moment from 'moment'
import BarChart from './components/Body/chart.jsx'
import './App.css';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [
        {
          instalacoes: [],
          instPeriodo: [],
          label: [],
          param: ""
        }],
      header: {
        title: "Minhas instalações",
        description: "Monitoramento dos pontos de venda com Integrador instalado",
      }
    }
  };

  componentDidMount() {
    //http://localhost:3006/instalacoes
    fetch(`http://nodejs-6-pv-painel.ipp.openshift.locawebcorp.com.br/instalacoes`,
      {
        method: 'get'
      }).then((response) => {
        return response.json()
      })
      .then((instalacoes) => {
        let range = {
          startDate: moment('2017-01-08'),
          endDate: moment()
        }
        instalacoes = instalacoes.map(inst => {
          return {
            npv: inst.npv,
            alias: inst.alias,
            pf: inst.pf,
            primeiroAbastecimento: inst.primeiroAbastecimento,
            ultimoAbastecimento: inst.ultimoAbastecimento,
            status: inst.status,
            tmpSAbst: moment().diff(moment(inst.ultimoAbastecimento), 'minutes')
          }
        });
        this.handlePeriodFilter(range, instalacoes);
      })
  }

  handlePeriodFilter(range, instalacoes) {
    let primDia = moment();
    let qtdDias = 1;

    if (instalacoes === undefined) {
      const history = this.state.history;
      instalacoes = history[1].instalacoes;
    }

    instalacoes = instalacoes.filter(inst => {
      return moment(inst.primeiroAbastecimento).isSameOrAfter(range.startDate) && moment(inst.primeiroAbastecimento).isSameOrBefore(range.endDate)
    })

    if (instalacoes.length > 0) {
      instalacoes = instalacoes.sort(this.ordenaPorData);
      qtdDias = moment(instalacoes[instalacoes.length - 1].primeiroAbastecimento).diff(moment(instalacoes[0].primeiroAbastecimento), 'day') + 3;
      primDia = moment(instalacoes[0].primeiroAbastecimento);
    }

    /* ===================== Obtem X e Y do Dashboard =================== */
    /*Gera array de datas*/
    let cont = 0;
    let datasEixoX = [];
    let param = 'day'

    if (qtdDias > 180) {
      qtdDias = Math.floor(qtdDias / 30) + 1;
      param = 'month'
    }

    do {
      datasEixoX.push((primDia.clone().add(cont, param)));
      cont++;
    } while (cont < qtdDias)

    instalacoes = instalacoes.filter(inst => inst.status !== -1);
    let instInativas = instalacoes.filter(inst => inst.tmpSAbst > 60);

    let instPeriodo = datasEixoX.map(date => { return (instalacoes.filter(inst => date.isSame(moment(inst.primeiroAbastecimento), param))).length })
    instInativas = datasEixoX.map(date => { return (instInativas.filter(inst => date.isSame(moment(inst.primeiroAbastecimento), param))).length })

    if (param === 'day') {
      datasEixoX = datasEixoX.map(day => day.format('DD/MM/YYYY'));
    }
    else {
      datasEixoX = datasEixoX.map(day => day.format('MMMM YYYY'));
    }

    const history = this.state.history;

    this.setState({
      history: history.concat([{
        instalacoes: instalacoes, //table
        instPeriodo: instPeriodo, //chart
        instInativas: instInativas, //chart
        labels: datasEixoX, //chart
        param: param === 'month' ? 'Instalações no mês' : 'Instalações no dia' //chart
      }])
    })
  }

  /*========================== Função auxiliar de comparação de objetos ======================= */
  ordenaPorData(a, b) {
    if (moment(a.primeiroAbastecimento).isBefore(moment(b.primeiroAbastecimento))) {
      return -1;
    }
    else if (moment(a.primeiroAbastecimento).isAfter(moment(b.primeiroAbastecimento))) {
      return 1;
    }
    return 0;
  }

  render() {
    const history = this.state.history[this.state.history.length - 1];
    return (
      <div className="App">
        <Header handleChild={this.handlePeriodFilter.bind(this)} data={this.state.header} />
        <BarChart instPeriodo={history.instPeriodo} instInativas={history.instInativas} labels={history.labels} param={history.param} />
        <Table instalacoes={history.instalacoes} />
      </div>
    )
  }

}

export default App;
