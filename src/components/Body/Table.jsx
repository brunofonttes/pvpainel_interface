﻿import React, { Component } from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import filterFactory, { textFilter, selectFilter } from 'react-bootstrap-table2-filter';
import cellEditFactory, { Type } from 'react-bootstrap-table2-editor';
import moment from 'moment-with-locales-es6';


const updatePV = (data) => {
  // http://localhost:3006/
  fetch(`http://nodejs-6-pv-painel.ipp.openshift.locawebcorp.com.br/updatepv`,
    {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(data)
    }).then((response) => {
      console.log(response);
    })
}

const headerFormatter = (column, colIndex, { sortElement, filterElement }) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      <div style={{ marginBottom: '0.8em' }}>
        {filterElement}
      </div>
      {column.text}
    </div>
  );
}

const dateFormatter = (cell, row) => {
  return (
    moment(cell).format('DD/MM/YYYY')
  );
}

const tmpFormatter = (cell, row) => {
  if (cell > 60 * 24 * 30) {
    cell = Math.floor(cell / (60 * 24 * 30));
    cell = (cell > 1) ? cell + ' meses' : cell + ' mês'
  }
  else if (cell > 60 * 24) {
    cell = Math.floor(cell / (60 * 24));
    cell = (cell > 1) ? cell + ' dias' : cell + ' dia'
  }
  else if (cell > 60) {
    cell = Math.floor(cell / 60);
    cell = (cell > 1) ? cell + ' horas' : cell + ' hora'
  }
  else {
    cell = (cell > 1) ? cell + ' minutos' : cell + ' minuto'
  }
  return cell;
}

const situacao = {
  "2": "Excluído",
  "0": "Bloqueado",
  "1": "Operando"
}

const tipo = {
  0: "-",
  1: 'KMV Caminhoneiro',
  2: 'Pró-Frotas',
  3: 'Ambos'
};

const customTotal = (from, to, size) => (
  <span className="react-bootstrap-table-pagination-total">
     Exibindo {from} de {to + 1} de {size} resultados
  </span>
);

const options = {
  hideSizePerPage: false,
  showTotal: true,
  paginationTotalRenderer: customTotal,
  sizePerPageList: [{
    text: '10', value: 10
  },
  {
    text: 'Todos', value: 500
  }
  ]
};

const defaultSorted = [{
  dataField: 'primeiroAbastecimento',
  order: 'desc'
}];

const columns = [
  {
    headerFormatter: headerFormatter,
    headerClasses: (column, colIndex) => { return 'customHeader' },
    dataField: 'npv',
    text: 'NPV Corporativo',
    headerAlign: 'center',
    align: 'center',
    sort: true,
    editable: false,
    filter: textFilter(),
  },
  {
    headerFormatter: headerFormatter,
    headerClasses: (column, colIndex) => { return 'customHeader' },
    dataField: 'alias',
    text: 'Apelido',
    headerAlign: 'center',
    align: 'center',
    sort: true,
    filter: textFilter()
  },
  {
    headerFormatter: headerFormatter,
    headerClasses: (column, colIndex) => { return 'customHeader' },
    dataField: 'primeiroAbastecimento',
    text: 'Primeira Transmissão',
    headerAlign: 'center',
    align: 'center',
    formatter: dateFormatter,
    sort: true,
    editable: false,
  },
  {
    headerFormatter: headerFormatter,
    headerClasses: (column, colIndex) => { return 'customHeader' },
    dataField: 'pf',
    text: 'Tipo',
    formatter: cell => tipo[cell],
    editor: {
      type: Type.SELECT,
      options: [{
        value: 0,
        label: '-'
      }, {
        value: 1,
        label: 'KMV Caminhoneiro'
      }, {
        value: 2,
        label: 'Pró-Frotas'
      }, {
        value: 3,
        label: 'Ambos',
      }]
    },
    headerAlign: 'center',
    align: 'center',

    filter: selectFilter({
      options: tipo,
    })
  },
  {
    headerFormatter: headerFormatter,
    headerClasses: (column, colIndex) => { return 'customHeader' },
    dataField: 'status',
    text: 'Situação',
    formatter: cell => situacao[cell],
    editor: {
      type: Type.SELECT,
      options: [
        {
          value: 2,
          label: 'Excluído'
        }, {
          value: 0,
          label: 'Bloqueado'
        }, {
          value: 1,
          label: 'Operando'
        },
      ]
    },
    headerAlign: 'center',
    align: 'center',
    filter: selectFilter({
      options: situacao,
    })
  },
  {
    headerFormatter: headerFormatter,
    headerClasses: (column, colIndex) => { return 'customHeader' },
    dataField: 'tmpSAbst',
    text: 'Última transmissão',
    formatter: tmpFormatter,
    headerAlign: 'center',
    align: 'center',
    sort: true,
    editable: false,
  }
];

class Table extends Component {
  render() {
    return (
      <div>
        <BootstrapTable
          // bootstrap4
          keyField='npv'
          data={this.props.instalacoes}
          columns={columns}

          pagination={paginationFactory(options)}
          filter={filterFactory()}

          cellEdit={cellEditFactory({
            mode: 'click',
            blurToSave: true,
            afterSaveCell: (oldValue, newValue, row, column) => { updatePV(row); }
          })}

          defaultSorted={defaultSorted}
          bordered={false}
        />
      </div>
    );
  }
}

export default Table;


