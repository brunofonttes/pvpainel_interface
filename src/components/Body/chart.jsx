import React, { Component } from 'react';
import { Bar } from 'react-chartjs-2';

class BarChart extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: {
                labels: [],
                datasets: [
                    {
                        label: [],
                        data: [],
                        backgroundColor: 'rgba(52, 152, 219, 0.4)',
                        borderColor: 'rgba(96, 125, 139, 1)',
                        borderWidth: 2,
                        hoverBackgroundColor: 'rgba(54, 162, 235, 0.4)',
                        hoverBorderColor: 'rgba(54, 162, 235, 1)',
                        
                    },
                    {
                        label: 'Total de instalações',
                        data: [],
                        tension: 0,
                        fill: false,
                        backgroundColor: 'rgba(108, 52, 131, 0.2)',
                        borderColor: 'rgba(108, 52, 131, 1)',
                        borderWidth: 2,
                        hoverBackgroundColor: 'rgba(24, 180, 44, 0.4)',
                        hoverBorderColor: 'rgba(24, 180, 44, 1)',
                        type: 'line',
                        hidden: true,
                    },
                    {
                        label: 'Total de instalações em atividade',
                        data: [],
                        tension: 0,
                        fill: false,
                        backgroundColor: 'rgba(24, 180, 44, 0.2)',
                        borderColor: 'rgba(24, 180, 44, 1)',
                        borderWidth: 2,
                        hoverBackgroundColor: 'rgba(24, 180, 44, 0.4)',
                        hoverBorderColor: 'rgba(24, 180, 44, 1)',
                        type: 'line',
                        hidden: true,
                    },
                    {
                        label: 'Instalações em inatividade',
                        data: [],
                        // tension: 0,
                        backgroundColor: 'rgba(255,99, 132, 0.4)',
                        borderColor: 'rgba(255,99, 132, 1)',
                        borderWidth: 2,
                        hoverBackgroundColor: 'rgba(255,99, 132, 0.4)',
                        hoverBorderColor: 'rgba(255,99, 132, 1)',
                        type: 'line'
                    },
                    {
                        label: 'Instalações em atividade',
                        data: [],
                        // tension: 0,
                        // fill: false,
                        backgroundColor: 'rgba(26, 188, 156, 0.4)',
                        borderColor: 'rgba(26, 188, 156, 1)',
                        borderWidth: 2,
                        hoverBackgroundColor: 'rgba(24, 180, 44, 0.4)',
                        hoverBorderColor: 'rgba(24, 180, 44, 1)',
                        type: 'line',
                        hidden: true,
                    },
                ]
            },
            option: {
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        }
    }

    componentWillReceiveProps(newProps) {
        if (newProps !== undefined) {
            let instTotVal = [];
            let instAtivas = []
            let instTotAtivas = []

            instTotVal.push(newProps.instPeriodo[0]);
            instAtivas.push(newProps.instPeriodo[0] - newProps.instInativas[0]);
            instTotAtivas.push(instAtivas[0]);
        
            for (let i = 1; i < newProps.instPeriodo.length; i++) {
                instTotVal.push(instTotVal[i - 1] + newProps.instPeriodo[i]);
                instAtivas.push(newProps.instPeriodo[i] - newProps.instInativas[i]);
                instTotAtivas.push(instTotAtivas[i - 1] + instAtivas[i]);
                console.log(instTotAtivas)
            }
            console.log(instAtivas)

            const state = { ...this.state }

            state.data.labels = newProps.labels

            state.data.datasets[0].data = newProps.instPeriodo;
            state.data.datasets[0].label = newProps.param;
            state.data.datasets[1].data = instTotVal;
            state.data.datasets[1].data = instTotVal;
            state.data.datasets[2].data = instTotAtivas;
            state.data.datasets[3].data = newProps.instInativas;
            state.data.datasets[4].data = instAtivas;

            this.setState({ state: state });
        }
    }

    render() {
        return (
            <div style={{ backgroundColor: 'white', height: '400px', marginBottom: '5vh', padding: '1.5%' }}>
                < Bar
                    data={this.state.data}
                    height={400}
                    options={this.state.option}
                    redraw
                />
            </div>
        )
    }
}

export default BarChart;