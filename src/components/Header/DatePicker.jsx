import React, { Component } from 'react';
import moment from 'moment-with-locales-es6';
import 'react-dates/initialize'
import 'react-dates-presets/demo/styles.css';

import DatePresetPicker from 'react-dates-presets';

class DatePicker extends Component {
    render() {
        const ranges = [
            { id: 'all', range: { startDate:moment('2017-07-01'), endDate:moment()}, label: 'Tudo' },
            { id: '7_days', range: { startDate: moment().subtract(7, 'days'), endDate: moment() }, label: 'Últimos 7 dias' },
            { id: '1_month', range: { startDate: moment().subtract(1, 'month'), endDate: moment() }, label: 'Último mês' },
            { id: '2_months', range: { startDate: moment().subtract(2, 'months'), endDate: moment() }, label: 'Últimos 2 meses' },
        ];

        return (
            <DatePresetPicker
                startDateId = {moment().format('DD/MM/YYYY')}
                endDateId = {moment().format('DD/MM/YYYY')}
                ranges={ranges}
                onChange={this.props.handleFilter}
                isOutsideRange={(date) => false}
            />
        );
    }
}

export default DatePicker
