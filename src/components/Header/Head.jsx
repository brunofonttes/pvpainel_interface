import React, { Component } from 'react';

const Stitle = { 
    fontWeight: 'bold', 
    color: '#121f3e' 
};
const Sdescription = { 
    color: '#b0bac9' 
};

class Head extends Component {
    render() {
        const {title, description} = this.props;
    
        return (
            <div className="head">
                <h2 style={Stitle}>{title}</h2>
                <p style={Sdescription}>{description}</p>
            </div>
        )
    }
}
export default Head;
