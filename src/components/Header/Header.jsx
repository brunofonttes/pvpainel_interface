import React, { Component } from 'react';
import Head from './Head.jsx';
import ReactPlaceholder from 'react-placeholder';
import 'react-dates/lib/css/_datepicker.css';
import DatePicker from './DatePicker.jsx';

import "react-placeholder/lib/reactPlaceholder.css";


class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            description: '',
            ready: false
        }
    };

    componentDidMount() {
        setTimeout(() => {
            this.setState({
                title: this.props.data.title,
                description: this.props.data.description,
                ready: true
            })
        }, 1000)
    }

    handleChildFilter(range) {
        this.props.handleChild(range);
    }

    render() {
        const { title, description, ready } = this.state
        const head = {
            marginBottom: '30px',
            width: '460px',
            height: '70px'
        }

        return (
            <div className="container">
                <div className="row">
                    <div className="header" style={head}>
                        <ReactPlaceholder
                            showLoadingAnimation
                            color="#e3e3e3"
                            type='text' rows={2}
                            ready={ready}
                            style={{ paddingTop: '1.5em' }}
                        >
                            <Head
                                title={title}
                                description={description}
                            />
                        </ReactPlaceholder>
                    </div>
                    <div className="col-sm">
                        <DatePicker
                            handleFilter={this.handleChildFilter.bind(this)}
                            style={{ float: 'right' }}
                        />
                    </div>
                </div>
            </div>

        )
    }
}
export default Header;
